<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<?php require_once("../head.html"); ?>
<meta name="description" content="大和綾瀬薬剤師会の会員薬局紹介です。" />
<title>会員薬局紹介 | 大和綾瀬薬剤師会</title>
<script>
$(function(){
});
</script>
</head>
<body id="idx" class="members">
<?php require_once("../header.html"); ?>
<div id="wrapper">

	<div id="contents" class="clearfix">

		<div id="main">
			<h1 id="ttl"><img src="images/ttl.png" alt="会員薬局紹介" /></h1>
			<div id="main_in" class="inner">
				<div class="pnavi_wrap">
					<ul class="pnavi">
					<li><a href="#area1">つきみ野・中央林間</a></li>
					<li><a href="#area2">南林間</a></li>
					<li><a href="#area3">鶴間</a></li>
					<li><a href="#area4">大和駅周辺</a></li>
					<li><a href="#area5">相模大塚</a></li>
					<li><a href="#area6">桜ケ丘</a></li>
					<li><a href="#area7">高座渋谷</a></li>
					<li><a href="#area8">綾瀬市</a></li>
					</ul>
				</div>
				<section>
<?php
$xml = simplexml_load_file("members.xml");
$areacnt=0;
foreach($xml->person as $list):
?>

<?php
if(strcmp($area,$list->area)!=0):
	$areacnt++;
?>
					<?php if($areacnt > 1): ?></ul><?php endif;?>
					<h1 class="mi1" id="area<?php echo $areacnt;?>"><?php echo $list->area;?></h1>
					<ul>
<?php endif; ?>
<?php $area = $list->area; ?>
					<li class="clearfix">
						<p class="name"><a href="detail.php?id=<?php echo $list->id;?>"><?php echo $list->name;?></a></p>
						<dl class="data">
						<dt class="adr">住所</dt><dd class="adr"><?php echo $list->adr;?></dd>
						<dt class="tel">TEL</dt><dd class="tel"><?php echo $list->tel;?></dd>
						</dl>
					</li>
<?php endforeach; ?>
					</ul>
				</section>
			</div>
		</div>
		<!--main-->

<?php require_once("../navi.html"); ?>

	</div>
	<!--contents-->


</div>
<!--wrapper-->
<?php require_once("../footer.html"); ?>
</body>
</html>
