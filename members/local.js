var map;
$().ready(function(){
	var myLatlng = new google.maps.LatLng(0,0);
	var myOptions = {
	  zoom: 17,
	  center: myLatlng,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	var marker = new google.maps.Marker({
	    position: new google.maps.LatLng(0,0),
	    map: map
	});
});

var my_google_map;
var my_google_geo;
var lat_num=""
var lng_num="";

function googlemap_init( id_name, addr_name, lat, lng ) {
		var latlng = new google.maps.LatLng(41, 133);
    var opts = {
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: latlng
    };
    my_google_map = new google.maps.Map(document.getElementById(id_name), opts);

    my_google_geo = new google.maps.Geocoder();

    var req = {
        address: addr_name ,

    };
    my_google_geo.geocode(req, geoResultCallback);

	if(lat != "") lat_num = lat;
	if(lng != "") lng_num = lng;

}


function geoResultCallback(result, status) {
	if (status != google.maps.GeocoderStatus.OK) {
		alert(status);
		return;
	}
	var latlng = result[0].geometry.location;
	if(lat_num != "")
	{
		latlng = new google.maps.LatLng(lat_num,lng_num);
	}

	my_google_map.setCenter(latlng);
	var marker = new google.maps.Marker({position:latlng, map:my_google_map, title:latlng.toString(), draggable:true});
	google.maps.event.addListener(marker, 'dragend', function(event){
		marker.setTitle(event.latLng.toString());
	});
}
