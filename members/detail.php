<?php
$xml = simplexml_load_file("members.xml");
foreach($xml->person as $list){
	if($list->id == $_GET['id'])
	{
		$area = $list->area;
		$id = $list->id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<?php require_once("../head.html"); ?>
<meta name="description" content="大和綾瀬薬剤師会の<?php echo $list->name;?>の紹介です。" />
<title>会員薬局紹介：<?php echo $list->name;?> | 大和綾瀬薬剤師会</title>
</head>
<body id="detail" class="members">
<?php require_once("../header.html"); ?>
<div id="wrapper">

	<div id="contents" class="clearfix">

		<div id="main">
			<div id="ttl"><img src="images/ttl.png" alt="会員薬局紹介" /></div>
			<div id="main_in" class="inner">
				<div class="back"><a href="./">一覧に戻る</a></div>
				<h1 class="mi1"><?php echo $list->name;?></h1>
				<?php if(!empty($list->comment)): ?><p class="comment"><?php echo $list->comment;?></p><?php endif;?>

				<div class="clearfix<?php if(!empty($list->picture)): ?> pic<?php endif;?>">
					<?php if(!empty($list->picture)): ?><img src="images/<?php echo $list->id;?>.jpg" class="picture" alt="<?php echo $list->name;?>" /><?php endif;?>
					<table class="data">
					<tr><th class="adr">住所</th><td class="adr"><?php echo $list->adr;?></td></tr>
					<tr><th class="tel">TEL</th><td class="tel"><?php echo $list->tel;?></td></tr>
					<?php if(!empty($list->fax)): ?><tr><th class="fax">FAX</th><td class="time"><?php echo $list->fax;?></td></tr><?php endif;?>
					<?php if(!empty($list->time)):
						$a_time = explode("\n", $list->time);
						foreach($a_time as $time) $time_tag .= "<span>".$time."</span>";
					 ?>
						<tr><th class="time">営業時間</th><td class="time"><?php echo $time_tag;?></td></tr>
					<?php endif;?>
					<?php if(!empty($list->holiday)): ?><tr><th class="holiday">定休日</th><td class="holiday"><?php echo $list->holiday;?><span class="note">（年末年始などのお休みはお店にお問い合わせください。）</span></td></tr><?php endif;?>
					<?php if(!empty($list->access)): ?><tr><th class="holiday">交通</th><td class="access"><?php echo $list->access;?></td></tr><?php endif;?>
					<?php if(!empty($list->parking)): ?><tr><th class="parking">駐車場</th><td class="parking"><?php echo $list->parking;?></td></tr><?php endif;?>
					<?php if(!empty($list->url)): ?><tr><th class="url">URL</th><td class="url"><a href="<?php echo $list->url;?>" target="_blank"><?php echo $list->url;?></a></td></tr><?php endif;?>
					</table>
				</div>

				<div class="map">
					<?php
					if(!empty($list->mapquery)){
						$map_query = $list->mapquery;
					}else if(!empty($list->lat)){
						$map_query = $list->lat.','.$list->lng;
					}else{
						$map_query = $list->name.','.$list->adr;
					}
					 ?>
					<iframe width="600" height="450" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDwgwaRsYXMDs9Uato-itu_N58JSqP5j7A&q=<?php echo $map_query;?>" allowfullscreen></iframe>
				</div>
				<!-- <div id="map_canvas">MAP</div> -->

<?php
		continue;
	}
}
?>


<?php
foreach($xml->person as $list)
{
	if(strcmp($area,$list->area)==0 && $id != $list->id)
	{
$tr .= <<< EOF
					<tr>
					<td class="name"><a href="detail.php?id={$list->id}">{$list->name}</a></td>
					<td class="adr">{$list->adr}</td>
					<td class="tel">{$list->tel}</td>
					</tr>
EOF;
	}
}
?>

<?php if(!empty($tr)): ?>
				<div class="nearby">
					<h2>近くの薬局</h2>
					<table>
<?php echo $tr; ?>
					</table>
				</div>
<?php endif; ?>


				<div class="pnavi_wrap">
					<div class="pnavi">
						<ul>
						<li><a href="./#area1">つきみ野・中央林間</a></li>
						<li><a href="./#area2">南林間</a></li>
						<li><a href="./#area3">鶴間</a></li>
						<li><a href="./#area4">大和駅周辺</a></li>
						<li><a href="./#area5">相模大塚</a></li>
						<li><a href="./#area6">桜ケ丘</a></li>
						<li><a href="./#area7">高座渋谷</a></li>
						<li><a href="./#area8">綾瀬市</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--main-->

<?php require_once("../navi.html"); ?>

	</div>
	<!--contents-->


</div>
<!--wrapper-->
<?php require_once("../footer.html"); ?>
</body>
</html>
