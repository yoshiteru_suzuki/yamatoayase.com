var isHome=false; if(location.pathname == "/" || location.pathname == "/index.html") isHome = true;
var ie = (function(){
	var undef, v = 3, div = document.createElement('div');
	while (div.innerHTML = '<!--[if gt IE '+(++v)+']><i></i><![endif]-->',div.getElementsByTagName('i')[0]);
	return v> 4 ? v : undef;
}());

var agent = navigator.userAgent;
var smart = false;
if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
	smart = true;
}

$(window).bind("orientationchange",function(){
});

$(window).scroll(function() {
	pagetop();
}); // scroll



$().ready(function(){

	if($(window).width() <= 768) smart = true;

	smartRollover();

	if(!smart) $('#pagetop').SmoothScroll();

	if(ie < 9)
	{
		$('img').each(function() {if($(this).attr('src').indexOf('.png') != -1) {$(this).css({'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +$(this).attr('src') +'", sizingMethod="scale");'});}});
	}


	$(window).bind("resize",function(){
		if(!ie || ie >= 9) setWindowSize();
	}); // resize

	$(window).bind("load",function(){

		setWindowSize();

		//sns button
		$('#sns_btn_fb').socialbutton('facebook_like', {url: 'https://yamatoayase.com/', button: 'button_count', width: 270});

		pagetop();

	}); // load


	if(!smart){

		/* アンカー */
		if($('.pnavi').size() > 0)
		{
			var nav = $('.pnavi');
			var navTop = nav.offset().top;
			$(window).scroll(function () {
				var winTop = $(this).scrollTop();
				if (winTop >= navTop) {
					nav.addClass('fixed');
				} else if (winTop <= navTop) {
					nav.removeClass('fixed');
				}
			});

			var anchor_h = $('.pnavi_wrap').height();
			if(ie && ie < 9) anchor_h = anchor_h-20;
			$('.pnavi a').click(function(){
				var targetOffset = parseInt(($($(this).attr("href")).offset().top) - anchor_h);

				var scldurat = 1200;
				$('html,body').stop().animate({scrollTop: targetOffset}, {duration: scldurat, easing: "easeInOutExpo"});
				return false;
			});

			$(window).on('load',function(){
				var hash = location.hash;
				if($(hash).size()){
					if(ie && ie < 9)
					{
						setTimeout(function(){
							$('html,body').stop().animate({scrollTop: parseInt($(hash).offset().top) - anchor_h}, {duration: 100});
						}, 50);
					}
					else
					{
						$('html,body').scrollTop(parseInt($(hash).offset().top) - anchor_h);
					}
				}
			});

		}

		$("#gnavi li a").hover(function(){

			var span_w;

			if($(this).parent("li").attr("id") == "gnavi_about") span_w = 166;
			else if($(this).parent("li").attr("id") == "gnavi_activities") span_w = 122;
			else if($(this).parent("li").attr("id") == "gnavi_emergency") span_w = 260;
			else if($(this).parent("li").attr("id") == "gnavi_members") span_w = 166;

			$("span",this).stop(true,true).animate({width:span_w},500,"easeOutExpo");
		},function(){
			$("span",this).stop(true,true).animate({width:0},500,"easeOutExpo");
		});
	}

	function setWindowSize(){

		if(smart) return;

		var bgnum = 1+Math.floor( Math.random() * 2 );
		var bgurl=$("#bg").css("background-image");
		bgurl = bgurl.replace(/_./,"_"+bgnum);
		$("#bg").css({backgroundImage:bgurl});

		var wh = $(window).height();
		var mh = $("#main_in").height();
		var fh = $("#footer").height()+parseInt($("#footer").css("padding-top"))+parseInt($("#footer").css("padding-bottom"));
		var mp = parseInt($("#main_in").css("padding-top"))+parseInt($("#main_in").css("padding-bottom"));
		var moff = $("#main_in").offset();
		var mtop = parseInt(moff.top);
		var mh_fix = ((wh-(mh+mtop)-mp-fh)+mh);
		if(wh > mh+mtop-mp-fh) $("#main_in").css("min-height",mh_fix+"px");

		$("#bg").css({visibility: "visible", left:parseInt($(window).width()/2)-169, height: $("#wrapper").outerHeight()});
		$("#footer").css({visibility: "visible"});

	}


	/*
	 アコーディオン
	*/
	$('.accordion dd').each(function() {
		$(this).height($('.in',this).height());
		$(this).hide();
	});

	$('.accordion dt').click(function() {
		var $this = $(this);
		var act = $this.hasClass('active');

		var targetOffset = $(this).offset().top;
		$('html,body').animate({scrollTop: targetOffset},400,"easeInOutQuart");

//		$('.accordion dt').removeClass('active');
//		$('.accordion dd').slideUp();

		if (!act)
		{
			$this.addClass('active');
			$this.next(".accordion dd").slideDown();
		}
		else
		{
			$this.removeClass('active');
			$this.next(".accordion dd").slideUp();
		}
	});


	function setGlobalNavi(){
		$body = $('body');
		$btn = $('.header-navbtn');
		$btn.click(function(){
			if ($body.hasClass('navopen')){
				$body.removeClass('navopen');
				$body.addClass('navclose');
			}else{
				$body.addClass('navopen');
				$body.removeClass('navclose');
			}
		});
	}
	setGlobalNavi();


}); // ready

function pagetop(){
	if(!smart)
	{
		var stop = $(window).scrollTop();
		if (stop < 10) {
			$("#pagetop").fadeOut(500);
		}else{
			$("#pagetop").fadeIn(500);
		}
	}
}


function smartRollover() {
	$("img, input").each(function (){
		if($(this).attr("src"))
		{
			if($(this).attr("src").match("_default."))
			{
				$('<img/>').attr( 'src', $(this).attr("src").replace("_default.", "_over.") );
				$(this).mouseover(
					function() {
						$(this).attr("src", $(this).attr("src").replace("_default.", "_over."));
					}
				);
				$(this).mouseout(
					function() {
						$(this).attr("src", $(this).attr("src").replace("_over.", "_default."));
					}
				);
			}
		}
	});
}



/*
 * Google Analytics
 */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-39613905-1', 'yamatoayase.com');
ga('send', 'pageview');
